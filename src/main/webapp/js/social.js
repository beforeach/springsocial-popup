var $document;

$( function() {
    $document = $( document );
    $document.on( 'click', '.js-social-login', loginSocial );
} );

/**
 * Calculate center of window
 * @returns {{w: number, h: number, left: number, top: number}}
 */
function calculatePopupDimensions() {
    var w = 650;
    var h = 650;
    var wLeft = window.screenLeft ? window.screenLeft : window.screenX;
    var wTop = window.screenTop ? window.screenTop : window.screenY;

    var left = wLeft + (window.innerWidth / 2) - (w / 2);
    var top = wTop + (window.innerHeight / 2) - (h / 2);
    return {w: w, h: h, left: left, top: top};
}

/**
 * Open popup to /signin/{provider}/popup
 * @param e
 */
function loginSocial( e ) {
    e.preventDefault();

    var triggerElement = $( e.target );
    var __ret = calculatePopupDimensions();
    var provider = triggerElement.attr( 'data-social-provider' );

    window.open( '/spring-social-quickstart/signin/' + provider + '/popup', "_blank", 'scrollbars=yes, width=' + __ret.w + ', height=' + __ret.h + ', top=' + __ret.top + ', left=' + __ret.left );
}